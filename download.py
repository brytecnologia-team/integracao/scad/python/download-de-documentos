import requests
import json
import csv
import time
import jwt
from pathlib import Path

"""
Get an access token using OAuth2 client credentials.
"""
def get_access_token(service_host, client_id, client_secret):
    print(f"Autenticando...")
    token_url = 'https://'+service_host+'/token-service/jwt'
    payload = {
        "grant_type": "client_credentials",
        "client_id": client_id,
        "client_secret": client_secret,
    }
    response = requests.post(token_url, data=payload)
    if response.status_code == 200:
        access_token = response.json()["access_token"]
        print(f"Autenticado!")
        return access_token
    else:
        print(f"Falha ao autenticar na API: {response.content}")
        return None

"""
Download signatures and protocols of a given workflow key using an OAuth2 access token.
"""
def download(service_host, access_token, wkey, directory):
    print(f"Buscando coleta {wkey}...")
    headers = {
        "Authorization": f"Bearer {access_token}",
        "Content-Type": "application/json",
    }
    
    status_response = requests.get('https://'+service_host+'/scad/rest/coletas/'+wkey, headers=headers, params=None)
    if status_response.status_code == 200:
        status_response_data = json.loads(status_response.content)
        if status_response_data['situacao']['chave'] == 'PENDENTE':
            print('Pendente de assinatura')
            return False
    else:
        print(f"Falha ao consultar situação da coleta: {response.content}")
        return False

    Path(directory).mkdir(parents=True, exist_ok=True)
    docs_response = requests.get('https://'+service_host+'/scad/rest/coletas/'+wkey+'/documentos', headers=headers, params=None)
    if docs_response.status_code == 200:
        docs_response_data = json.loads(docs_response.content)
        params=[]
        for doc_data in docs_response_data:
            params.append(('chaveDocumento', doc_data['chaveDocumento']))
      
        print(f"Realizando download protocolo {wkey}...")
        response = requests.get('https://'+service_host+'/scad/rest/documentos/'+wkey+'/protocolos', headers=headers, params=params)
        if response.status_code == 200:
            print(f"Download do protocolo realizado!")
            with open(directory+"/"+wkey + "_protocolo.zip", "wb") as f:
                f.write(response.content)
        else:
            print(f"Falha ao realizar download do protocolo: {response.content}")
            return False
        print(f"Realizando download assinatura {wkey}...")
        response = requests.get('https://'+service_host+'/scad/rest/documentos/'+wkey+'/assinaturas', headers=headers, params=params)
        if response.status_code == 200:
            print(f"Download das assinaturas realizado!")
            with open(directory+"/"+wkey + "_assinatura.zip", "wb") as f:
                f.write(response.content)
        else:
            print(f"Falha ao realizar download das assinaturas: {response.content}")
            return False
    return True



def main(csv_file, config_file):
    with open(config_file, "r") as f:
        config = json.load(f)

    service_host = config["service_host"]
    access_token = get_access_token(service_host, config["client_id"], config["client_secret"])

    processed_rows = [];
    with open(csv_file, newline="", encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if row['downloaded'] != 'True':
                wkey=row['wkey']
                directory=row['directory']
                decoded_token = jwt.decode(access_token, options={"verify_signature": False}, algorithms=["RS256"])
                """print("Validade token: "+str(decoded_token['exp'])+" agora: "+str(int(time.time())))"""
                if decoded_token['exp'] < int(time.time()):
                    access_token = get_access_token(service_host, config["client_id"], config["client_secret"])
                if download(service_host, access_token, wkey, directory):
                    row['downloaded'] = 'True'
                else:
                    row['downloaded'] = 'False'
            processed_rows.append(row);
    fieldnames = list(processed_rows[0].keys())
    with open(csv_file, 'w', newline='', encoding='utf-8') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(processed_rows)
    print('Download finalizado')

if __name__ == "__main__":
    import sys

    if len(sys.argv) < 2:
        print("Usage: python download.py [csv_file]")
        sys.exit(1)

    csv_file = sys.argv[1]
    config_file = "config.json"
    try:
        main(csv_file, config_file)
    except Exception as e:
        print(f"An error occurred: {str(e)}")
        sys.exit(1)