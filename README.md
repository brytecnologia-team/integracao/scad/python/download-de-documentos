# Download de Documentos

## Introdução

Este exemplo permite o download do relatório de assinatura (se ativado) e do documento assinado para coletas de assinatura existentes na Assinatura em Grupo. Uma planilha (CSV) é utilizada como entrada e controle dos downloads realizados, podendo cada linha receber uma coleta e uma pasta de destino para armazenamento. 

## Como utilizar

- Faça o download dos arquivos download.py, config.json e teste_download.csv
- Edite o arquivo config.json, ajustando as configurações do client_id e client_secret da sua aplicação cadastrada no BRy Cloud.
- Edite o arquivo teste_download.csv, incluíndo a chave da coleta e o diretório de destino de cada coleta. 

- Execute:  

```
python3 download.py teste_download.csv
```

## Formato de entrada

A planilha deve ter uma linha por coleta a ser consultada. 

- wkey: chave da coleta no BRy Cloud;
- directory: diretório onde os arquivos devem ser colocados. Se não existe, será criado. 
- downloaded: Deve iniciar como "False". Vai ser alterado para "True" caso o download ocorra com sucesso;

## Formato de saída 

A linha "downloaded" da planilha será alterada para "True" caso o download ocorra com sucesso. Dessa forma, você pode sempre usar a mesma planilha para continuar downloads de coletas ainda pendentes, pois as linhas onde "downloaded" for "True" não serão consultadas novamente em próximas execuções. 

Dentro da pasta de download de cada coleta serão criados até dois arquivos:

 - {wkey}_assinatura.zip: Com o documento assinado digital ou eletrônicamente;
 - {wkey}_protocolo.zip: Com o protocolo de assinaturas, caso tenha sido ativado para a coleta (opcional);
